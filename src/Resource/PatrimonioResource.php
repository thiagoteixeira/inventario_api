<?php

namespace App\Resource;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Model\Patrimonio;

class PatrimonioResource extends Patrimonio {
    
    private $em;
    private $msgErro;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    private function converterParaArray($dados)
    {
        if (!empty($dados)) {
            return get_object_vars($dados);
        }
    }
    
    public function obterDetalhes($patrimonio) 
    {
        try {            
            $sql = "SELECT u.UNIDADE, u.UNIDADE_ID, s.SETOR, s.SETOR_ID, f.FORNECEDOR, fa.FABRICANTE, m.MODELO, c.CATEGORIA, tp.TIPO_PATRIMONIO, p.PATRIMONIO, p.DESCRICAO, p.PATRIMONIO_ID, p.PRODUCT_KEY,
                       CASE 
                            WHEN p.SITUACAO = 1 THEN 'Alocado'
                            WHEN p.SITUACAO = 2 THEN 'Em Movimentação'
                            WHEN p.SITUACAO = 3 THEN 'Baixado'
                            WHEN p.SITUACAO = 4 THEN 'Assistência'
                            ELSE 'Novo'
                        END AS SITUACAO, p.NF, p.NUM_SERIE,p.VALOR_AQUISICAO,p.DT_AQUISICAO,p.TAXA_DEPRECIACAO,p.DEPRECIACAO_ACUMULADA,p.SALDO_RESIDUAL, p.USUARIO_RESP, p.USUARIO_CAD, p.DT_ATUALIZACAO,
                        p.DT_CADASTRO
                FROM patrimonios AS p
                LEFT JOIN fornecedores AS f ON ( p.FORNECEDOR_ID = f.FORNECEDOR_ID )
                LEFT JOIN fabricantes AS fa ON ( p.FABRICANTE_ID = fa.FABRICANTE_ID )
                LEFT JOIN modelos AS m ON ( p.MODELO_ID = m.MODELO_ID )
                INNER JOIN categorias AS c ON ( p.CATEGORIA_ID = c.CATEGORIA_ID )
                INNER JOIN tipo_patrimonio AS tp ON ( p.TIPO_PATRIMONIO_ID = tp.TIPOPATRIMONIO_ID )
                INNER JOIN setor_unidade AS su ON ( p.SETORUNIDADE_ID = su.SETORUNIDADE_ID )
                INNER JOIN setores AS s ON ( su.SETOR_ID = s.SETOR_ID )             
                INNER JOIN unidades AS u ON ( su.UNIDADE_ID = u.UNIDADE_ID )
                WHERE p.PATRIMONIO = $patrimonio";
            
            return $this->em->getConnection()->query($sql)->fetchAll();                            
        } catch (exception $e) {
            $this->msgErro = $e->getMessage();
            return false;
        }        
    }    
}
