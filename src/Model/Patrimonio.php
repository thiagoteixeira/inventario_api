<?php

namespace App\Model;

use App\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="patrimonios")
 */
 class Patrimonio {
     
     /**
      * @ORM\Id
      * @ORM\Column(name="PATRIMONIO_ID",type="integer")
      */
     protected $id;
     
     /**
      * @ORM\Column(name="FORNECEDOR_ID")
      */
     protected $fornecedor_id;
     
     /**
      * @ORM\Column(name="FABRICANTE_ID")
      */
     protected $fabricante_id;
     
     /**
      * @ORM\Column(name="MODELO_ID")
      */
     protected $modelo_id;
     
     /**
      * @ORM\Column(name="CATEGORIA_ID")
      */
     protected $categoria_id;
     
     /**
      * @ORM\Column(name="TIPO_PATRIMONIO_ID")
      */
     protected $tipo_patrimonio_id;
     
     /**
      * @ORM\Column(name="SETORUNIDADE_ID")
      */
     protected $setorunidade_id;
     
     /**
      * @ORM\Column(name="DESCRICAO")
      */
     protected $descricao;
     
     /**
      * @ORM\Column(name="SITUACAO")
      */
     protected $situacao;
     
     /**
      * @ORM\Column(name="NF")
      */
     protected $nf;
     
     /**
      * @ORM\Column(name="PRODUCT_KEY")
      */
     protected $product_key;
     
     /**
      * @ORM\Column(name="NUM_SERIE")
      */
     protected $num_serie;
     
     /**
      * @ORM\Column(name="VALOR_AQUISICAO")
      */
     protected $valor_aquisicao;
     
     /**
      * @ORM\Column(name="DT_AQUISICAO")
      */
     protected $dt_aquisicao;
     
     /**
      * @ORM\Column(name="TAXA_DEPRECIACAO")
      */
     protected $taxa_depreciacao;
     
     /**
      * @ORM\Column(name="DEPRECIACAO_ACUMULADA")
      */
     protected $depreciacao_acumulada;
     
     /**
      * @ORM\Column(name="SALDO_RESIDUAL")
      */
     protected $saldo_residual;
     
     /**
      * @ORM\Column(name="USUARIO_RESP")
      */
     protected $usuario_resp;
     
     /**
      * @ORM\Column(name="USUARIO_CAD")
      */
     protected $usuario_cad;
     
     /**
      * @ORM\Column(name="DT_ATUALIZACAO")
      */
     protected $dt_atualizacao;
     
     /**
      * @ORM\Column(name="DT_CADASTRO")
      */
     protected $dt_cadastro;
     
     /**
      * @ORM\Column(type="string")
      */
      protected $patrimonio;
      
      public function getId()
      {
          return $this->id;
      }
      
      public function setPatrimonio($patrimonio)
      {
          $this->patrimonio = $patrimonio;
      }
      
      public function getPatrimonio()
      {
          return $this->getPatrimonio();
      }
}
