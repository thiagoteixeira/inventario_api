<?php

namespace App\Model;

use App\Model;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fabricantes")
 */
 class Fabricante {
     
     /**
      * @ORM\Id
      * @ORM\Column(name="FABRICANTE_ID",type="integer")
      */
     protected $fabricante_id;
     
     /**
      * @ORM\Column(name="FABRICANTE")
      */
     protected $fabricante;
     
     /**
      * @ORM\Column(name="SITUACAO")
      */
     protected $situacao;
     
     /**
      * @ORM\Column(name="SITUACAO")
      */
     protected $usuario;
     
     /**
      * @ORM\Column(name="DT_ATUALIZACAO")
      */
     protected $dt_atualizacao;
     
     public function setFabricante($fabricante)
     {
         $this->fabricante = $fabricante;
     }
     
     public function getFabricante()
     {
         return $this->fabricante;
     }
     
     public function setSituacao($situacao)
     {
         $this->situacao = $situacao;
     }
     
     public function getSituacao()
     {
         return $this->situacao;
     }
     
     public function setUsuario($usuario)
     {
         $this->usuario = $usuario;
     } 
     
     public function getUsuario()
     {
         return $this->usuario;
     }
     
     public function setDtAtualizacao($data)
     {
         $this->dt_atualizacao = $data;
     }
     
     public function getDtAtualizacao()
     {
         return $this->dt_atualizacao;
     }
}
