<?php
// Routes
namespace App;
use App;
use App\Resource\PatrimonioResource;


$app->get('/patrimonio/[{patrimonio}]', function ($request, $response, $args) {
                
    $numPatrimonio = $request->getAttribute('patrimonio');
    
    if (is_numeric($numPatrimonio)) {
        if (strlen($numPatrimonio) <= 6) {
            $numPatrimonio = ltrim($numPatrimonio,0);
            $objPatrimonio = new App\Resource\PatrimonioResource($this->get('em'));
            $detalhesDoPatrimonio = $objPatrimonio->obterDetalhes($numPatrimonio);  
            if (empty($detalhesDoPatrimonio) || !isset($numPatrimonio)) {
                return $response->getBody()->write('');
            } else if ($detalhesDoPatrimonio == false) {
                return $response->getBody()->write(false);
            } else {
                return $response->withJson($detalhesDoPatrimonio);    
            }
        } else {
            return $response->getBody()->write('Parâmetro deve conter no máximo 6 caracteres'); 
        }        
    } else {
       return $response->getBody()->write('Parâmetro deve ser um número'); 
    }       
});
